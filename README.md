# Login System with Python Flask and MySQL
## Things todo list:
1. Clone this repository: `git clone https://gitlab.com/flask-application/login-system-with-python-flask-and-mysql.git`
2. Go to your folder: `cd login-system-with-python-flask-and-mysql`
3. Run command: `virtualenv flasklogin`
4. Activate it: `source flasklogin/bin/activate`
5. Create new database and change in `app.py`
6. Run command: `pip install requirements.txt`
7. Run command: `set FLASK_APP=app.py`
8. Run command: `set FLASK_DEBUG=1`
9. Run command: `flask run`
10. Open your favorite browser: http://localhost:5000/login

## Screen shot

Home Page

![Home Page](img/home.png "Home Page")

Register Page

![Register Page](img/register.png "Register Page")

Login Page

![Login Page](img/login.png "Login Page")

Profile Page

![Profile Page](img/profile.png "Profile Page")